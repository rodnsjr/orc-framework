package oRCResources;

import java.util.List;

public class ORCFileHandler extends ORCResourceHandler {
	private List<ORCFileResource> fileResources;
	
	public ORCFileHandler(List<ORCFileResource> fileResources){
		this.fileResources = fileResources;
	}

	public int getNumberOfFiles() {
		// TODO Auto-generated method stub
		return fileResources.size();
	}
	
	public ORCFileResource getFileResource(int index)
	{
		return fileResources.get(index);
	}
}
