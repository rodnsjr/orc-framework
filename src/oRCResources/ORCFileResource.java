package oRCResources;

import java.io.File;
import java.io.InputStream;

public class ORCFileResource extends ORCResource{
	
	protected File file;
	protected InputStream inputStream;
	
	public ORCFileResource(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public ORCFileResource(String name, File file)
	{
		this(name);
		this.file = file;
	}
	
	public ORCFileResource(String name, File file, InputStream inputStream)
	{
		this(name);
		this.file = file;
		this.inputStream = inputStream;
	}
	
	public File getFile() {
		return file;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	

}
