package oRCResources;

public class ORCResource {
	private static int totalResourcesLoaded = 0;
	
	protected String name;
	protected int id;
	
	public ORCResource(String name)
	{
		this.name = name;
		totalResourcesLoaded++;
		id = totalResourcesLoaded;
	}
	
	public ORCResource(int id, String name)
	{
		this(name);
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getId()
	{
		return id;
	}
}
