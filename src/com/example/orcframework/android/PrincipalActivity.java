package com.example.orcframework.android;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import commons.ORCActivity;
import commons.ORCImage;

import oRC.CascadeObjectRecognition;
import oRC.NeuralNetworkRecognition;
import oRC.ORCRecognition;
import oRCMachineLearning.ORCNeuralNetwork;
import oRCResources.*;

public class PrincipalActivity extends ORCActivity {
	
	boolean firstLoad = false;
    private static int RESULT_LOAD_IMAGE = 1;
    private Bitmap image;
    private TextView tv1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		setContentView(R.layout.principal);    
		Button buttonLoadImage = (Button) findViewById(R.id.buttonLoadPicture);
		tv1 = (TextView) findViewById(R.id.textView1);
	    buttonLoadImage.setOnClickListener(new View.OnClickListener() {
	    
	        @Override
	        public void onClick(View arg0) {
	             
	            Intent i = new Intent(
	                    Intent.ACTION_PICK,
	                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	             
	            startActivityForResult(i, RESULT_LOAD_IMAGE);
	        }
	    });
	    Button b1 = (Button)findViewById(R.id.button1);
	    b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ORCImage myImage = null;
				if (image != null)
					myImage = objectRecognizer.recognizeImage(image);
		        
				if (myImage.hasObjects())
				{
					ImageView imageView = (ImageView) findViewById(R.id.imgView);
					imageView.setImageBitmap(myImage.getImageObject());
					tv1.setText(myImage.getImageLabel().replaceAll(".xml", ""));
					//imageView.setImageBitmap(myImage.getOriginalEntryImage());
					//tv1.setText(myImage.getImageLabel() + " " + myImage.getCertaintyIndex());					
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
     
	    if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data)
	    {
	        Uri selectedImage = data.getData();
	        String[] filePathColumn = { MediaStore.Images.Media.DATA };
	
	        Cursor cursor = getContentResolver().query(selectedImage,
	                filePathColumn, null, null, null);
	        cursor.moveToFirst();
	
	        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	        String picturePath = cursor.getString(columnIndex);
	        cursor.close();
	         
	        ImageView imageView = (ImageView) findViewById(R.id.imgView);
	        imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
	        image = BitmapFactory.decodeFile(picturePath);
	      
     
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}
	
	protected int[] getAllClassifierFiles(){
		Field[] f = R.raw.class.getFields();
		int[] rawResources = new int[f.length];
		int count = 0;	
		try
		{
			for (Field i : f) 
			{
				rawResources[count] = i.getInt(i);
				count++;
			}
		}
		catch (Exception e)
		{
			Log.d("ORCException", e.getMessage());
		}
		return rawResources;
	}
	
	@Override
	protected ORCFileHandler getClassifiers() {
		
		
		ORCFileHandler of = null;
		int[] rawResources = getAllClassifierFiles();
		
		List<ORCFileResource> files = new ArrayList<ORCFileResource>();
		for (int fileId : rawResources) {
			InputStream is = this.getResources().openRawResource(fileId);
			String resName = this.getResources().getResourceEntryName(fileId) + ".xml";
			File cascadeDir = this.getDir("cascade", Context.MODE_PRIVATE);
			File cascadeClassifierFile = new File(cascadeDir, resName);
			ORCFileResource fr = new ORCFileResource(cascadeClassifierFile.getName(), 
							     cascadeClassifierFile, is);
			files.add(fr);
		}
		
		/*
		List<ORCFileResource> files = new ArrayList<ORCFileResource>();
		ORCFileHandler of;
		InputStream is = this.getResources().openRawResource(R.raw.nt);
		File file = new File(getResources().getResourceName(R.raw.nt) + ".nnet");
		files.add(new ORCFileResource("Neural Network", file, is));		
		*/
		
		of = new ORCFileHandler(files);		
		return of;
	}
	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
	    @Override
	    public void onManagerConnected(int status) {
	        switch (status) {
	            case LoaderCallbackInterface.SUCCESS:
	            {
	                Log.i("ORCSuccess", "OpenCV loaded successfully");
	                if (!firstLoad)
	                {
	                	Log.d("ORCDebug", "FirstLoad");
	                	firstLoad = true;
	                	onLoadRecognition(new CascadeObjectRecognition(classifierFiles));
	                }
	            } break;
	            default:
	            {
	                super.onManagerConnected(status);
	            } break;
	        }
	    }
	};

	@Override
	protected void onLoadRecognition(ORCRecognition objectRecognizer) {
		this.objectRecognizer = objectRecognizer;
	}

	@Override
	protected void onInitORC(ORCFileHandler classifiers) {
		//load opencv or whatever other meanings of you might need
		//e.g... server connection
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_7, this, mLoaderCallback);
		Log.i("ORCDebug", "OnORCCreate");
		
		//load the files you'll need
		this.classifierFiles = classifiers;
		
		//inicializa o componente de reconhecimento
		//objectRecognizer = new NeuralNetworkRecognition(classifiers);
		
		//some say ...
		//ORCNeuralNetwork.gambiarra = this;
	}

}
