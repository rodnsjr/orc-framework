package commons;
import java.io.File;
import java.io.IOException;

import oRC.ORCRecognition;
import oRCResources.ORCFileHandler;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.GetChars;

public abstract class ORCActivity extends Activity {
	
	protected ORCRecognition objectRecognizer;
	protected ORCFileHandler classifierFiles;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	    onInitORC(getClassifiers());
	}
	
	public File getTemporaryFile(String ext)
	{
		File outputDir = getCacheDir(); // context being the Activity pointer
		File outputFile = null;
		try {
			outputFile = File.createTempFile("tmp", ext ,outputDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		return outputFile;
	}
	
	protected abstract void onInitORC(ORCFileHandler classifiers);
	
	protected abstract void onLoadRecognition(ORCRecognition objectRecognizer);

	protected abstract ORCFileHandler getClassifiers();

}
