package commons;

import java.util.ArrayList;
import java.util.List;

import oRCImageProcessing.ORCImageProcess;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * @author Rodney
 * Object Recognized and Classified Image ...
 */
public class ORCImage {
	//The image data
	private Bitmap imageData;
	//The classified objects of the image (e.g. Car, Truck)
	private List<String> imageLabels;
	//The image of the objects (e.g. the car image, the truck image)
	private List<Bitmap> imageObjects;
	
	private List<String> certaintyIndex;
	
	private Bitmap originalEntryImage;
	
	/**
	 * Create an image ready for recognition
	 * @param image - the image to be recognized
	 */
	public ORCImage(Bitmap image){
		this.originalEntryImage = image;
		this.imageData = image;
		imageLabels = new ArrayList<String>();
		imageObjects = new ArrayList<Bitmap>();
		this.certaintyIndex = new ArrayList<String>();
	}
	
	/**
	 * Create an object recognized and classified image
	 * @param image - the actual image
	 * @param imageLabels - the label of the objects in the image (e.g. Car, Truck)
	 * @param imageObjects - the objects of the image (e.g. the car image, the truck image)
	 */
	public ORCImage(Bitmap image, List<String> imageLabels, List<Bitmap> imageObjects)
	{
		this.imageData = image;
		this.imageLabels = imageLabels;
		this.imageObjects = imageObjects;
		
	}
	
	public void processImage(ORCImageProcess pc)
	{
		Log.d("ORCDebug", "processing image with: " + pc.getClass());
		imageData = pc.processImage(imageData);
	}
	
	public void addData(String imageLabel){
		imageLabels.add(imageLabel);
	}
	
	public void addData(String imageLabel, String certaintyIndex){
		imageLabels.add(imageLabel);
		this.certaintyIndex.add(certaintyIndex);
	}
	
	public void addData(String imageLabel, Bitmap imageObject)
	{
		imageLabels.add(imageLabel);
		imageObjects.add(imageObject);
	}
	
	public void addData(String imageLabel, List<Bitmap> imageObjects)
	{
		imageLabels.add(imageLabel);
		for (Bitmap bitmap : imageObjects) {
			this.imageObjects.add(bitmap);
		}
	}
	
	public Bitmap getImageData() {
		return imageData;
	}

	public List<String> getImageLabels() {
		return imageLabels;
	}
	
	public String getImageLabel() {
		return imageLabels.get(0);
	}
	
	public String getCertaintyIndex(){
		return certaintyIndex.get(0);
	}


	public List<Bitmap> getImageObjects() {
		return imageObjects;
	}
	
	public Bitmap getImageObject() {
		return imageObjects.get(0);
	}
	
	public boolean hasObjects()
	{
		if ((!imageObjects.isEmpty()) || (!imageLabels.isEmpty()))
			return true;
		return false;
	}

	public Bitmap getOriginalEntryImage() {
		return originalEntryImage;
	}
	
}
