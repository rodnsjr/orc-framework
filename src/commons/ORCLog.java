package commons;

import android.util.Log;

public class ORCLog {
	public static String debug = "ORCDebug";
	public static String exception = "ORCException";
	public static String sucess = "ORCSucess";
	
	public static void printSucess(String phrase)
	{
		Log.i(sucess, phrase);
	}
	
	public static void printException(String phrase)
	{
		Log.e(exception, phrase);
	}
	
	public static void printDebug(String phrase)
	{
		Log.d(debug, phrase);
	}
}
