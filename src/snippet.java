import oRC.ORCRecognition;
import oRCImageProcessing.ORCImageProcess;
import oRCImageProcessing.ORCImageResizing;
import oRCMachineLearning.ORCClassifier;
import oRCResources.ORCResourceHandler;
import commons.ORCImage;

import android.graphics.Bitmap;


public class snippet extends ORCRecognition{
	
	public snippet(ORCResourceHandler classifierResourceHandler) {
		super(classifierResourceHandler);
		// TODO Auto-generated constructor stub
	}

	ORCImageProcess orcImageProcess;
	
	public ORCImage recognizeImage(Bitmap imagem){
		//estrutura a imagem em um componente ORC
		ORCImage myORCImage = new ORCImage(imagem);
		
		//define um novo tamanho para a imagem
		int altura = 640, largura = 480;
		//define o componente de processamento que altera o tamanho
		orcImageProcess = new ORCImageResizing(largura, altura);
		
		//processa a imagem
		myORCImage.processImage(orcImageProcess);
		
		boolean busca = false;
		//para cada classificador, buscar um objeto, caso encontrado a flag ser� true
		for (ORCClassifier classifier : classifiers) {
			busca = classifier.findObjects(myORCImage);
		}
		//se foram encontrados objetos retornar a estrutura completa
		if (busca)
			return myORCImage;
		//caso contr�rio retornar um valor nulo
		else
			return null;
	}

	@Override
	protected void onInitRecognition() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void createClassifiers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String classifyImage(Bitmap image) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
