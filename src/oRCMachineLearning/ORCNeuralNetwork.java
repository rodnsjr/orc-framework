package oRCMachineLearning;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import oRCResources.ORCFileResource;
import oRCResources.ORCResource;

import org.apache.commons.io.FilenameUtils;
import org.neuroph.contrib.imgrec.ImageRecognitionPlugin;
import org.neuroph.contrib.imgrec.image.Image;
import org.neuroph.contrib.imgrec.image.ImageFactory;
import org.neuroph.core.NeuralNetwork;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.Log;

import commons.ORCActivity;
import commons.ORCImage;
import commons.ORCLog;

public class ORCNeuralNetwork extends ORCClassifier implements Runnable {

	private ORCFileResource classifierFile;

	private NeuralNetwork nnet;
	private ImageRecognitionPlugin imageRecognition;
	public static ORCActivity gambiarra;

	public ORCNeuralNetwork(ORCResource classifierResource){
		super(classifierResource);

	}

	@SuppressWarnings("unused")
	private String[] recognize(Image image) {
                // recognize image
		HashMap<String, Double> output = imageRecognition.recognizeImage(image);
		return getAnswer(output);
	}

	private void loadData() {
                // load neural network in separate thread with stack size = 32000
		Log.d("ORCDebug", "loadData()");
		Thread t = new Thread(null, this, "name", 64000);
		t.start();
		
	}

        
	private String[] getAnswer(HashMap<String, Double> output) {
		double highest = 0;
		String answer = "";
		for (Map.Entry<String, Double> entry : output.entrySet()) {
                    if (entry.getValue() > highest) {
				highest = entry.getValue();
				answer = entry.getKey();
			}
		}
		String[] exit = new String[2];
		exit[0] = answer;
		exit[1] = "" + highest;
		return exit;
	}

	@Override
	public boolean findObjects(ORCImage image) {
		// TODO Auto-generated method stub
		
		if (gambiarra == null)
		{
			ORCLog.printException("You must use GAMBIARRA!!!!!");
			return false;	
		}
		
		try {
			
			File outputFile = gambiarra.getTemporaryFile(".jpg");
			
			FileOutputStream fos = new FileOutputStream(outputFile);
			
			image.getImageData().compress(Bitmap.CompressFormat.JPEG, 100, fos);
			
			Image neurophImg = ImageFactory.getImage(outputFile);
			
			String[] exit = recognize(neurophImg);
			
			if (Double.parseDouble(exit[1]) < 0.3)
			{
				return false;
			}
			else
			{
				image.addData(exit[0], exit[1]);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return false;
	}
	
	@Override
	protected void loadClassifier() {
		if (classifierResource instanceof ORCFileResource)
		{
			classifierFile = (ORCFileResource) classifierResource;
		}
		else
			classifierFile = null;
		String ext = "";
		if (classifierFile != null)
			ext = FilenameUtils.getExtension(classifierFile.getFile().getPath());
		
		if (!ext.contains("nnet"))
		{
			Log.e("ORCException", "Invalid Neural Network File");
			return;
		
		}
		if (classifierFile != null)
			loadData();
	}

	@Override
	public void run() {
        // open neural network
		Log.d("ORCDebug", "loadingThread()");
		InputStream is = classifierFile.getInputStream();
                    // load neural network
		nnet = NeuralNetwork.load(is);
		imageRecognition = (ImageRecognitionPlugin) nnet.getPlugin(ImageRecognitionPlugin.class);
        // dismiss loading dialog
		Log.d("ORCDebug", "ANN Loaded");
	}
}