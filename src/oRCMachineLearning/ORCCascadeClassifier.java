package oRCMachineLearning;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import oRCImageProcessing.Convertions;
import oRCResources.ORCFileResource;
import oRCResources.ORCResource;

import org.apache.commons.io.FilenameUtils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;


import commons.ORCImage;

import android.graphics.Bitmap;
import android.util.Log;


public class ORCCascadeClassifier extends ORCClassifier{

	private CascadeClassifier openCVCascadeClassifier;
	//private Scalar rectColor = new Scalar(0, 255, 0, 255);
	private InputStream openedRaw;
	private ORCFileResource classifierFile;
	
	public ORCCascadeClassifier(ORCResource classifierFile) {
		super(classifierFile);
	}
	
	public boolean findObjects(ORCImage image){
		Log.i("ORCDebug", "Finding Objects");
		
		Mat m = Convertions.convertToMat(image.getImageData());
        
		MatOfRect objects = new MatOfRect();

        if (openCVCascadeClassifier != null){
                openCVCascadeClassifier.detectMultiScale(m, objects, 1.1, 2, 2,
                		// TODO: objdetect.CV_HAAR_SCALE_IMAGE
                        new Size(0, 0.2f), new Size());
        }
        
        if (!objects.empty())
        {
        	Log.i("ORCSuccess", "Objects Found !!!");
            Rect[] objectsArray = objects.toArray();
            
            /*
            for (int i = 0; i < objectsArray.length; i++)
                Core.rectangle(m, objectsArray[i].tl(), objectsArray[i].br(), rectColor, 3);
            */
    		
            String objectsDetected = classifierResource.getName();
            
            List<Bitmap> detectedObjects = new ArrayList<Bitmap>(objectsArray.length);
            for (Rect dtObj : objectsArray) {
                Mat obj = new Mat(m, dtObj);
                detectedObjects.add(Convertions.convertToBitmap(obj));  
			}
            
            image.addData(objectsDetected, detectedObjects);
            
            return true;
        }
        else
        {
        	Log.d("ORCException", "Objects Not Found");
        	return false;
        }
	}
	
	@Override
	protected void loadClassifier() {		
		
		if (classifierResource instanceof ORCFileResource)
		{
			this.classifierFile = (ORCFileResource) classifierResource;
			openedRaw = classifierFile.getInputStream();
		}
		else
			classifierFile = null;
		String ext = FilenameUtils.getExtension(classifierFile.getFile().getAbsolutePath());
		if (!ext.contains("xml"))
		{
			Log.e("ORCException", "Invalid Cascade Classifier File");
			return;
		}
		
		try
		{
	        FileOutputStream os = new FileOutputStream(classifierFile.getFile().getAbsoluteFile());	
	        byte[] buffer = new byte[4096];
	        int bytesRead;
	        while ((bytesRead = openedRaw.read(buffer)) != -1) {
	            os.write(buffer, 0, bytesRead);
	        }
	        openedRaw.close();
	        os.close();
	        
	        Log.d("ORCDebug",classifierFile.getFile().getAbsolutePath());
	        
			openCVCascadeClassifier = new CascadeClassifier(classifierFile.getFile().getAbsolutePath());
			
			Log.d("ORCSuccess","Cascade Classifier " + classifierFile.getName() + " Loaded Successfully");
		}
		catch (Exception e)
		{
			Log.d("ORCException", e.getMessage());
		}
	}

}
