package oRCMachineLearning;

import oRCResources.ORCResource;

import commons.ORCImage;


public abstract class ORCClassifier {
	
	protected ORCResource classifierResource;
	
	public ORCClassifier(ORCResource classifierResource)
	{
		this.classifierResource = classifierResource;
		loadClassifier();
	}
	
	public String getName()
	{
		return classifierResource.getName();
	}
	
	public abstract boolean findObjects(ORCImage image);

	protected abstract void loadClassifier();
	
}
