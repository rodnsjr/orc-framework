package oRC;

import java.util.ArrayList;
import java.util.List;

import commons.ORCImage;

import oRCMachineLearning.*;
import oRCResources.ORCFileHandler;
import oRCResources.ORCResourceHandler;


import android.graphics.Bitmap;
import android.util.Log;

public class CascadeObjectRecognition extends ORCRecognition{
	
	ORCFileHandler classifierFileHandler;
	
	public CascadeObjectRecognition(ORCResourceHandler classifierFileHandler)
	{
		super(classifierFileHandler);
	}
	
	public String classifyImage(Bitmap image){
		String exit = "";
		Log.d("ORCDebug", "Classifying Image ...");
		
		ORCImage exitImage = new ORCImage(image);
		for (ORCClassifier actualClassifier : classifiers) {
			if (actualClassifier.findObjects(exitImage))
			{
				exit = exitImage.getImageLabel();
				Log.d("ORCDebug", "Found something : " + exit);
			}
			
		}
		
		if (exit != "")
		{
			Log.i("ORCSucess", "Classify Success !!!");
		}
		else
			Log.d("ORCException", "Image Cannot Be Classified");
		return exit;
	}
	
	public ORCImage recognizeImage(Bitmap image)
	{
		String exit = "";
		Log.d("ORCDebug", "Recognizing Image ...");
		
		ORCImage exitImage = new ORCImage(image);
		for (ORCClassifier actualClassifier : classifiers) {
			
			//more objects might be found and added on the image ...
			if (actualClassifier.findObjects(exitImage))
			{
				exit = exitImage.getImageLabel();
				Log.d("ORCDebug", "Found something : " + exit);
			}
		}
		
		return exitImage;
	}

	
	public List<String> getClassifierNames()
	{
		List<String> st = new ArrayList<String>(classifiers.size());
		
		for (ORCClassifier classifierName : classifiers) {
			st.add(classifierName.getName());
		}
		
		return st;
	}
	
	protected void onInitRecognition()
	{
		if (classifierResourceHandler instanceof ORCFileHandler)
		{
			this.classifierFileHandler = (ORCFileHandler)classifierResourceHandler;
		}
		classifiers = new ArrayList<ORCClassifier>(classifierFileHandler.getNumberOfFiles());
	}
	
	protected void createClassifiers()
	{
		for (int i = 0; i < classifierFileHandler.getNumberOfFiles(); i++) {
			classifiers.add(new ORCCascadeClassifier(classifierFileHandler.getFileResource(i)));			
		}
	}
	
}
