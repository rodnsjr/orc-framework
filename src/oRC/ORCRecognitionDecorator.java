package oRC;

import android.graphics.Bitmap;

import commons.ORCImage;

import oRCResources.ORCResourceHandler;

public class ORCRecognitionDecorator extends ORCRecognition{

	protected ORCRecognition decorator;
	
	public ORCRecognitionDecorator(ORCResourceHandler classifierResourceHandler) {
		super(classifierResourceHandler);
		// TODO Auto-generated constructor stub
	}
	
	public ORCRecognitionDecorator(ORCResourceHandler classifierResourceHandler, ORCRecognition decorator)
	{
		this(classifierResourceHandler);
		this.decorator = decorator;
	}

	@Override
	protected void onInitRecognition() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void createClassifiers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String classifyImage(Bitmap image) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ORCImage recognizeImage(Bitmap image) {
		// TODO Auto-generated method stub
		return null;
	}

}
