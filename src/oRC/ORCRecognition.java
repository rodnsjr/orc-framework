package oRC;

import java.util.List;

import commons.ORCImage;

import oRCMachineLearning.ORCClassifier;
import oRCResources.ORCFileHandler;
import oRCResources.ORCResourceHandler;

import android.graphics.Bitmap;


public abstract class ORCRecognition {
	
	protected List<ORCClassifier> classifiers;
	protected ORCResourceHandler classifierResourceHandler;
	
	public ORCRecognition(ORCResourceHandler classifierResourceHandler)
	{
		this.classifierResourceHandler = classifierResourceHandler;
		onInitRecognition();
		createClassifiers();
	}
	
	protected abstract void onInitRecognition();
	protected abstract void createClassifiers();
	
	public List<ORCClassifier> getClassifierList()
	{
		return classifiers;
	}
	
	public abstract String classifyImage(Bitmap image);
	
	public abstract ORCImage recognizeImage(Bitmap image);
}
