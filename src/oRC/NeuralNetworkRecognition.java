package oRC;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.util.Log;

import commons.ORCActivity;
import commons.ORCImage;
import commons.ORCLog;

import oRCImageProcessing.ORCImageResizing;
import oRCImageProcessing.OpenCVCannyEdgeDetector;
import oRCMachineLearning.ORCClassifier;
import oRCMachineLearning.ORCNeuralNetwork;
import oRCResources.ORCFileHandler;
import oRCResources.ORCResourceHandler;

public class NeuralNetworkRecognition extends ORCRecognition {
	
	private ORCFileHandler handler;
	
	public NeuralNetworkRecognition(ORCResourceHandler classifierFileHandler) {
		super(classifierFileHandler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onInitRecognition() {
		// TODO Auto-generated method stub
		if (classifierResourceHandler instanceof ORCFileHandler)
		{
			this.handler = (ORCFileHandler) classifierResourceHandler;
			classifiers = new ArrayList<ORCClassifier>();
		}
		else
			handler = null;
		
	}

	@Override
	protected void createClassifiers() {
		// TODO Auto-generated method stub
		Log.d("ORCDebug", "files = " + handler.getNumberOfFiles());
		for (int i = 0; i < handler.getNumberOfFiles(); i++)
		{
			ORCNeuralNetwork nn = new ORCNeuralNetwork(handler.getFileResource(i));
			classifiers.add(nn);
		}
	}

	@Override
	public String classifyImage(Bitmap image) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ORCImage recognizeImage(Bitmap image) {
		// TODO Auto-generated method stub
		
		Log.d("ORCDebug", "NeuralNetwork Recognize Image");
		
		ORCImage orcImage = new ORCImage(image);
		
		orcImage.processImage(new OpenCVCannyEdgeDetector(new ORCImageResizing(30, 30)));
		
		if (classifiers.get(0).findObjects(orcImage)){
			ORCLog.printSucess("Image Found!!");
		}
		
		return orcImage;
	}

}
