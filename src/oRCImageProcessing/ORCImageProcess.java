package oRCImageProcessing;

import android.graphics.Bitmap;

public abstract class ORCImageProcess {
	public abstract Bitmap processImage(Bitmap image);
}
