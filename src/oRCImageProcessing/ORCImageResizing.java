package oRCImageProcessing;

import android.graphics.Bitmap;

public class ORCImageResizing extends ORCImageProcess {
	private Bitmap image;
	private int width, height;
	public ORCImageResizing(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
	
	@Override
	public Bitmap processImage(Bitmap image) {
		// TODO Auto-generated method stub
		resizeImage(image);
		return this.image;
	}
	
	private void resizeImage(Bitmap image)
	{
		this.image = Bitmap.createScaledBitmap(image, height, width, true);
	}

}
