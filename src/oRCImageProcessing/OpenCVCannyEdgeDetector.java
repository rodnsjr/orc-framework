package oRCImageProcessing;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.graphics.Bitmap;

public class OpenCVCannyEdgeDetector extends EdgeDetectorDecorator{
	private ORCImageProcess imageProcess;
	private Bitmap image;
	
	public OpenCVCannyEdgeDetector(ORCImageProcess decorator)
	{
		this.imageProcess = decorator;
	}
	
	@Override
	public Bitmap processImage(Bitmap image)
	{
		if (imageProcess != null)
			this.image = imageProcess.processImage(image);
		
		edgeDetection(this.image);
		return this.image;		
	}
	
	@Override
	protected void edgeDetection(Bitmap image) {
		// TODO Auto-generated method stub
		Mat mImg = Convertions.convertToMat(image);
		Mat mGray = new Mat(mImg.rows(), mImg.cols(), CvType.CV_8UC1, new Scalar(0));
		Imgproc.cvtColor(mImg , mGray, Imgproc.COLOR_BGRA2GRAY, 4); 
        //Applying Canny
        Imgproc.Canny(mGray, mGray, 80, 90);
        this.image = Convertions.convertToBitmap(mGray);
	}

}
