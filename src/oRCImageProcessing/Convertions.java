package oRCImageProcessing;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

import android.graphics.Bitmap;

public class Convertions {
	
	public static Mat convertToMat(Bitmap image)
	{
		//First convert Bitmap to Mat
		Mat ImageMat = new Mat ( image.getHeight(), image.getWidth(), CvType.CV_8U, new Scalar(4));
		Bitmap myBitmap32 = image.copy(Bitmap.Config.ARGB_8888, true);
		Utils.bitmapToMat(myBitmap32, ImageMat);
		
		return ImageMat;
	}
	
	public static Bitmap convertToBitmap(Mat ImageMat)
	{
		//Then convert the processed Mat to Bitmap
		Bitmap resultBitmap = Bitmap.createBitmap(ImageMat.cols(),  ImageMat.rows(),Bitmap.Config.ARGB_8888);;
		Utils.matToBitmap(ImageMat, resultBitmap);
		
		return resultBitmap;
	}
	
}
